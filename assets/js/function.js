// JavaScript Document
$(document).ready(function(e) {
			$(".device-nav").click(function(){
				$(this).find('span').toggleClass('fa-times').toggleClass('fa-bars');
				$("nav").toggleClass("reveal");
			});
				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
			
					if (scroll >= 100) {
						$('header').addClass('fixed');
					} else {
						$('header').removeClass('fixed');
					}
				});
/*** Testmonial Slider ***/
$('#testmonial').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dots: true,
  infinite: false,
  speed: 2000,
  autoplay: true,
  prevArrow: $('.prev2'),
  nextArrow: $('.next2'),
  /*responsive: [{
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
  ]*/

});
});
